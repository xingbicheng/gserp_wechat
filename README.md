一、WePY项目的目录结构
====

```
├── dist                   小程序运行代码目录（该目录由WePY的build指令自动编译生成，请不要直接修改该目录下的文件）
├── node_modules           
├── src                    代码编写的目录（该目录为使用WePY后的开发目录）
|   ├── components         WePY组件目录（组件不属于完整页面，仅供完整页面或其他组件引用）
|   |   ├── com_a.wpy      可复用的WePY组件a
|   |   └── com_b.wpy      可复用的WePY组件b
|   ├── config             项目配置文件
|   |   ├── api.js         后台接口映射表
|   |   ├── ip.js          后台请求ip地址
|   |   └── config.js      项目参数配置文件（请求方式等）
|   ├── assets             放置静态文件（图片、字体包等）
|   |   ├── images         静态图片
|   |   └── fonts          字体包
|   ├── request            
|   |   ├── requestMethod  请求方法
|   |   └── request.js     使用策略模式分发请求方式
|   ├── service            
|   |   ├── login.js       登录接口请求函数
|   |   └── other.js       其他接口请求函数
|   ├── utils                          
|   |   ├── error.js       错误code映射表
|   |   ├── utils.js       共有函数
|   |   └── tokenHandler.js token处理
|   ├── zanui              小程序ui框架            
|   ├── pages              WePY页面目录（属于完整页面）
|   |   ├── index.wpy      index页面（经build后，会在dist目录下的pages目录生成index.js、index.json、index.wxml和index.wxss文件）
|   |   └── other.wpy      other页面（经build后，会在dist目录下的pages目录生成other.js、other.json、other.wxml和other.wxss文件）
|   └── app.wpy            小程序配置项（全局数据、样式、声明钩子等；经build后，会在dist目录下生成app.js、app.json和app.wxss文件）
├── wepy.config.js         打包配置文档
└── package.json           项目的package配置

```

二、项目运行流程
====
此流程为页面加载到请求完成

一、app.wpy
---------

    app.wpy为小程序的入口文件，在这个文件中
    ① 配置路由`config.pages`
    **注意：**在路由中默认加载第一个路由
    ```
    pages: [
      'pages/mine/test', // 开始默认加载这个
      'pages/mine/index',
      'pages/homepage/index',  
      'pages/sign/index'
    ],
    ```
    ② 配置整个小程序的样式
    **注意：**是配置全局的样式，如果页面不单独配置是默认采用这种配置
    ```
    window: {
       backgroundTextStyle: 'light',
       navigationBarBackgroundColor: '#fff',
       navigationBarTextStyle: 'black'
    },
    ```    
    ③ 配置底部tabBar
    ```
    tabBar: {
      list: [{
        pagePath: 'pages/mine/test',
        iconPath:'assets/images/qw-homepage.png',
        selectedIconPath:'assets/images/qw-homepage-select.png',
        text: '首页'
      }, {
        pagePath: 'pages/sign/index',
        iconPath:'assets/images/sign.png',
        selectedIconPath:'assets/images/sign-select.png',
        text: '签到'
      }, {
        pagePath: 'pages/mine/index',
        iconPath:'assets/images/mine.png',
        selectedIconPath:'assets/images/mine-select.png',
        text: '我的'
      }]
    }
    ```
    ④ 全局公有数据
    
    ```
    globalData = {
       userInfo: null
    }
    ```
    

二、pages/
----

WePY页面目录（属于完整页面）

三、service/
---------
后台接口服务这个模块对应这后台接口服务。
  
```
import request from '../request/request'
import Config from '../config/api'
import { createTheURL } from '../utils/utils'
export async function Login(params) {
  return request(createTheURL(Config.API.USER, 'login'), {
    method: 'POST',
    data: params
  })
}
```
**注意**

1.私有接口必须为全小写

2.通常情况下一个业务对应一个services

四、request/
----------
向后台发送请求
在这个里面一共有4个文件

1.request.js

选择调用的后台请求模式，axios或者fetch，固定参数`API.REQUEST_METHOD`可以在`./../../config/api`中进行配置

2.requestMethod/
各种不同的请求方式


我们在处理后台请求时还会需要几个文件：

 1. config/ip.js设置请求的ip地址
 
 2. utils/utils.js中的createTheURL拼接ip地址与API接口，使其成为一个完整的请求地址
 
 3. config/api.js写共有的请求方式（REQUEST_METHOD），和API请求接口  
 
# 三、项目截图
- 登陆注册

  ![图片描述][2]
- 分类菜单
 
  ![图片描述][3]
- 购物车

   ![图片描述][4]
- 采购单

  ![图片描述][5]
  
# 四、项目参考网站
https://tencent.github.io/wepy/index.html  
https://developers.weixin.qq.com/miniprogram/dev/index.html  
https://youzan.github.io/zanui-weapp/#/zanui/base/icon  
http://www.cnblogs.com/shaoting/p/6051261.html  
https://weapp.iviewui.com/  
https://www.jianshu.com/p/c5f6c98b2685  
https://segmentfault.com/a/1190000016850107  
https://www.jianshu.com/p/0078507e14d3  

 [2]: https://image-static.segmentfault.com/271/956/2719562784-5c70ba5fa0ef0_articlex
  [3]: https://image-static.segmentfault.com/177/406/1774061214-5c70ba8c6f181_articlex
  [4]: https://image-static.segmentfault.com/387/556/3875563460-5c70baa08a86f_articlex
  [5]: https://image-static.segmentfault.com/342/073/3420730189-5c70bab78e3d7_articlex
