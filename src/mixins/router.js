import wepy from 'wepy'

export default class router extends wepy.mixin {
  data = {
    mixin: 'This is mixin data.'
  }
  methods = {
    // 路由跳转
    routeByCar(path, item) {
      console.log(path, item)
      if (path === 'car') {
        this.$root.$switch(`/pages/car/carList`)
      } else {
        this.$root.$navigate(`/pages/typeDetail/index?goodsTypeId=${item.goodsTypeId}&goodsType=${item.goodsType}`)
      }
    },
    routeToDetail (defaultType) {
      this.$root.$navigate(`/pages/typeDetail/index?goodsTypeId=${defaultType.goodsTypeId}&goodsType=${defaultType.goodsType}`)
    },
    routeToHomepage() {
      this.$root.$switch('/pages/category/index')
    },
    routeToConfirm() {
      this.$root.$navigate('/pages/car/confirm')
    },
    routeToOrderDeatil(id) {
      this.$root.$navigate('/pages/order/detail?id=' + id)
    },
    routeToOrder () {
      console.log(this.$root)
      this.$root.$switch(`/pages/order/orderList`)
    },
    routeToPurchase() {
      this.$root.$navigate('../purchase/orderList')
    },
    routeToPurchaseDeatil(id) {
      console.log(id)
      this.$root.$navigate('/pages/purchase/detail?id=' + id)
    },
    routeTogoodsDetail(id) {
      console.log(id)
      this.$root.$navigate('/pages/purchase/goodsDetail?id=' + id)
    }
  }
}
