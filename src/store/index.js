import { createStore, applyMiddleware } from 'redux'
import promiseMiddleware from 'redux-promise'
import rootReducer from './reducers'

export default function configStore () { // 初始化store
  // promiseMiddleware 是一个中间件，方便后面action做异步处理
  // reducer是纯函数，用于接收Action和当前state作为参数，返回新的state
  const store = createStore(rootReducer, applyMiddleware(promiseMiddleware))
  return store
}
