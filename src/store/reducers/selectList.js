import { handleActions } from 'redux-actions'
import { UPDATE_LIST, DELL_ALL_LIST, DELL_GOOD } from '../types/selectList'

const defaultState = {

}

function filter(obj, func) {
  let ret = {}
  for (let key in obj) {
    if (obj.hasOwnProperty(key) && func(obj[key], key)) {
      ret[key] = obj[key]
    }
  }
  return ret
}

export default handleActions({
  [UPDATE_LIST](state, action) { // 函数名为INCREMENT变量的值，改写法为ES6所支持的
    return {
      ...state,
      ...action.payload
    }
  },
  [DELL_GOOD](state, action) { // 函数名为INCREMENT变量的值，改写法为ES6所支持的
    return filter(state, (value, key) => {
      return key !== action.payload
    })
  },
  [DELL_ALL_LIST]() { // 函数名为INCREMENT变量的值，改写法为ES6所支持的
    return {

    }
  }
}, defaultState) // 接收2个参数，第一个为一个接收多个reducer的大对象，后一个为初始state
