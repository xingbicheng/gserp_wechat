import { handleActions } from 'redux-actions'
import { UPDATE_STATE } from '../types'

const defaultState = {

}

export default handleActions({
  [UPDATE_STATE](state, action) { // 函数名为INCREMENT变量的值，改写法为ES6所支持的
    return {
      ...state,
      ...action.payload
    }
  }
}, defaultState) // 接收2个参数，第一个为一个接收多个reducer的大对象，后一个为初始state
