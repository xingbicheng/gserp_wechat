import { handleActions } from 'redux-actions'
import { UPDATE_GOODS, DELETE_ALL_GOODS } from '../types/index'

const defaultState = [

]

export default handleActions({
  [UPDATE_GOODS](state, action) { // 函数名为INCREMENT变量的值，改写法为ES6所支持的
    return [
      ...state,
      ...action.payload
    ]
  },
  [DELETE_ALL_GOODS]() { // 函数名为INCREMENT变量的值，改写法为ES6所支持的
    return [ ]
  }
}, defaultState) // 接收2个参数，第一个为一个接收多个reducer的大对象，后一个为初始state
