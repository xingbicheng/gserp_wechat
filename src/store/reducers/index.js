import { combineReducers } from 'redux'
import selectList from './selectList'
import role from './role'
import register from './register'
import goods from './goods'

export default combineReducers({
  selectList,
  role,
  register,
  goods
})
