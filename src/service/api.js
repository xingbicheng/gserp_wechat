import request from '../request/request'
import Config from '../config/api'
import { createTheURL } from '../utils/utils'

async function GoodstypeList(params) {
  return request(createTheURL(Config.API.GOODSTYPE, 'list'), {
    method: 'GET',
    data: params
  })
}

async function GoodsList(params) {
  return request(createTheURL(Config.API.GOODS, 'list'), {
    method: 'GET',
    data: params
  })
}

async function GoodsListAll(params) {
  return request(createTheURL(Config.API.GOODS, 'all'), {
    method: 'GET',
    data: params
  })
}

async function GoodsGet(params) {
  return request(createTheURL(Config.API.GOODS, 'get'), {
    method: 'GET',
    data: params
  })
}

async function WXOrderList(params) {
  return request(createTheURL(Config.API.WX_ORDER, 'list'), {
    method: 'GET',
    data: params
  })
}

async function OrderGetnum(params) {
  return request(createTheURL(Config.API.ORDER, 'getnum'), {
    method: 'GET',
    data: params
  })
}

async function OrderGet(params) {
  return request(createTheURL(Config.API.ORDER, 'get'), {
    method: 'GET',
    data: params
  })
}

async function CarAdd(params) {
  return request(createTheURL(Config.API.CAR, 'add'), {
    method: 'POST',
    data: params
  })
}

async function CarEdit(params) {
  return request(createTheURL(Config.API.CAR, 'edit'), {
    method: 'POST',
    data: params
  })
}

async function CarList(params) {
  return request(createTheURL(Config.API.CAR,'list'),{
    method:'POST',
    data:params
  })
}

async function CarListAll(params) {
  return request(createTheURL(Config.API.CAR,'listAll'),{
    method:'POST',
    data:params
  })
}

async function CarDel(params) {
  return request(createTheURL(Config.API.CAR, 'delgoods'), {
    method: 'DELETE',
    data: params
  })
}

async function CarDelAll(params) {
  return request(createTheURL(Config.API.CAR, 'delAll?customerId=1'), {
    method: 'DELETE',
    data: params
  })
}

async function WXOrderAdd(params) {
  return request(createTheURL(Config.API.WX_ORDER, 'add'), {
    method: 'POST',
    data: params
  })
}

async function WXOrderGet(params) {
  return request(createTheURL(Config.API.WX_ORDER, 'get'), {
    method: 'GET',
    data: params
  })
}

async function addToCar(params) {
  return request(createTheURL(Config.API.CAR, 'addToCar'), {
    method: 'POST',
    data: params
  })
}

async function purchaseOrderList(params) {
  return request(createTheURL(Config.API.PurchaseOrder, 'list'), {
    method: 'GET',
    data: params
  })
}

async function purchaseOrderListDetail(params) {
  return request(createTheURL(Config.API.PurchaseOrder, 'get'), {
    method: 'GET',
    data: params
  })
}

async function plannedStorageGet(params) {
  return request(createTheURL(Config.API.PlannedStorage, 'get'), {
    method: 'GET',
    data: params
  })
}

async function plannedStorageAudit(params) {
  return request(createTheURL(Config.API.PlannedStorage, 'audit'), {
    method: 'GET',
    data: params
  })
}
async function plannedStorageUnAudit(params) {
  return request(createTheURL(Config.API.PlannedStorage, 'unaudit'), {
    method: 'GET',
    data: params
  })
}
async function getOpenId(params) {
  return request('https://api.weixin.qq.com/sns/jscode2session', {
    method: 'GET',
    json: true,
    data: params
  })
}

async function PurchasePriceList(params) {
  return request(createTheURL(Config.API.PURCHASE_PRICE, 'wxlist'), {
    method: 'GET',
    data: params
  })
}

async function PurchasePriceEdit(params) {
  return request(createTheURL(Config.API.PURCHASE_PRICE, 'wxedit'), {
    method: 'GET',
    data: params
  })
}

export default {
  GoodstypeList,
  GoodsList,
  GoodsListAll,
  GoodsGet,
  WXOrderList,
  OrderGetnum,
  OrderGet,
  CarAdd,
  CarEdit,
  CarList,
  CarListAll,
  CarDel,
  CarDelAll,
  WXOrderAdd,
  WXOrderGet,
  addToCar,
  purchaseOrderList,
  purchaseOrderListDetail,
  plannedStorageGet,
  plannedStorageAudit,
  plannedStorageUnAudit,
  getOpenId,
  PurchasePriceList,
  PurchasePriceEdit
}
