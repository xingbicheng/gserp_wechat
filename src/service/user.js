import request from '../request/request'
import Config from '../config/api'
import { createTheURL } from '../utils/utils'
export async function UserInfo(params) {
  return request(createTheURL(Config.API.USER, 'current'), {
    method: 'GET',
    data: params
  })
}

export async function UserStatus(params) {
  return request(createTheURL(Config.API.USER, `getopenid`), {
    method: 'GET',
    data: params
  })
}

export async function UserGet(params) {
  return request(createTheURL(Config.API.USER, 'get/byopenid'), {
    method: 'GET',
    data: params
  })
}

export async function UserAdd(params) {
  return request(createTheURL(Config.API.USER, 'add'), {
    method: 'POST',
    data: params
  })
}
