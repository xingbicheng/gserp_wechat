import wepy from 'wepy'
import tokenHandler from '@/utils/tokenHandler'

function request(url, option) {
  const newOptions = { // 建立一个请求包
    ...option,
    url
  }
  if (url.indexOf('/user/user/login') < 0) { // 如果api非login，则要传token
    newOptions.header = {
      'Content-Type': 'application/json',
      'token': tokenHandler.getToken() || null
    }
  } else {
    newOptions.header = { // 如果api是登陆页面，则不需要发token，因为用户还未获取到token
      'Content-Type': 'application/json'
    }
  }
  return new Promise((resolve, reject) => { // 新建一个promise，接收一个函数作为参数，该函数有两个参数
    // 一个参数是成功时调用的函数，另一个时被拒绝时调用的函数
    wepy.request({
      ...newOptions,
      success: function(res) {
        if (res.statusCode === 200) {
          resolve(res.data)
        } else {
          reject(res.data)
        }
      },
      fail: function(res) {
        reject(new Error('Network request failed'))
      }
    })
  })
}

export default request
