const API = {
  USER: '/api/sysWxUser',
  WECHAT: '/wechat',
  SIGN: '/sign/sign',
  LOGIN: '',
  GOODSTYPE: '/api/goodstype', // 商品类型
  GOODS: '/api/goods', // 货品
  WX_ORDER: '/api/wxorder',
  ORDER: '/api/order',
  CAR: '/api/car',
  PurchaseOrder: '/api/purchaseOrder',
  PlannedStorage: '/api/plannedStorage',
  PURCHASE_PRICE: '/api/wxpurchaseprice'
}
module.exports = {
  API
}
