import { remoteLinkAddress } from '../config/ip'

export const OriginCode = {
  MANUAL_ORDER: {
    text: '手工订单',
    value: 0
  },
  WEB_ORDER: {
    text: '网络订单',
    value: 1
  },
  WECHAT_ORDER: {
    text: '微信订单',
    value: 2
  }
}

export const NOT_REGISTERED = 0 // 未注册
export const NOT_REVIEW = 1 // 注册但未审核
export const REVIEW_SUCCESS = 2 // 注册并审核成功
export const REVIEW_FAILED = 3 // 注册但审核失败

export const PurchaseTypeCode = [
  {
    text: '订单采购',
    value: '0'
  },
  {
    text: '计划采购',
    value: '1'
  }
]

export function createTheURL(modelAPI, interfaceType) {
  const temp = modelAPI.split('')
  if (temp[temp.length - 1] !== '/') {
    temp.push('/')
  }
  const baseURL = remoteLinkAddress
  const IP = baseURL + temp.join('') + interfaceType
  return IP
}
function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}
export function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
class NumberAnimateClass {
  constructor(opt) {
    let def = {
      from: 0, //  开始时的数字
      speed: 2000, // 总时间
      refreshTime: 100, // 刷新一次的时间
      decimals: 2, // 小数点后的位数
      onUpdate: function() {}, // 更新时回调函数
      onComplete: function() {} // 完成时回调函数
    }
    this.tempValue = 0 //  累加变量值
    this.opt = Object.assign(def, opt) //  assign传入配置参数
    this.loopCount = 0  //  循环次数计数
    this.loops = Math.ceil(this.opt.speed / this.opt.refreshTime)//  数字累加次数
    this.increment = (this.opt.from / this.loops)//  每次累加的值
    this.interval = null  //  计时器对象
    this.init()
  }
  init() {
    this.interval = setInterval(() => { this.updateTimer() }, this.opt.refreshTime)
  }
  updateTimer() {
    this.loopCount++
    this.tempValue = this.formatFloat(this.tempValue, this.increment).toFixed(this.opt.decimals)
    if (this.loopCount >= this.loops) {
      clearInterval(this.interval)
      this.tempValue = this.opt.from
      this.opt.onComplete()
    }
    this.opt.onUpdate()
  }
  // 解决0.1+0.2不等于0.3的小数累加精度问题
  formatFloat(num1, num2) {
    let baseNum, baseNum1, baseNum2
    try {
      baseNum1 = num1.toString().split('.')[1].length
    } catch (e) {
      baseNum1 = 0
    }
    try {
      baseNum2 = num2.toString().split('.')[1].length
    } catch (e) {
      baseNum2 = 0
    }
    baseNum = Math.pow(10, Math.max(baseNum1, baseNum2))
    return (num1 * baseNum + num2 * baseNum) / baseNum
  };
}
export default NumberAnimateClass
