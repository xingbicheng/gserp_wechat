import wepy from 'wepy'

const OpenIdHandler = function() {

}
OpenIdHandler.prototype = {
  getOpenId() {
    return wepy.getStorageSync('openId')
  },
  setOpenId(value) {
    wepy.setStorageSync('openId', value)
  },
  remove() {
    wepy.removeStorageSync('openId')
  }
}
module.exports = new OpenIdHandler()
