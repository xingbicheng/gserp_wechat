import wepy from 'wepy'

const TokenHandler = function() {

}
TokenHandler.prototype = {
  getToken() {
    return wepy.getStorageSync('token')
  },
  setToken(value) {
    wepy.setStorageSync('token', value) // 将token缓存到本地 接收2个参数，key和value
  },
  removeToken() {
    wepy.removeStorageSync('token')
  }
}
module.exports = new TokenHandler()
