<style lang="less" scoped>
  ::-webkit-scrollbar
  {
    width: 0;
    height: 0;
    color: transparent;
  }

  .main-box{
    width: 100%;
    box-sizing: border-box;
    padding: 10px;
    text-align: left;
    .block{
      width: 100%;
      .title{
        font-size: 18px;
        width: 100%;
        height: 60px;
        line-height: 35px;
        font-weight: normal;
        text-align: center;
      }
      .detail{
        height: 60px;
        line-height: 60px;
      }
      .content{
        box-sizing: border-box;
        width: 100%;
        border-top: 1px solid #eeeeee;
        border-bottom: 1px solid #eeeeee;
        .tr{
          width: 80%;
          height: 25px;
          display: block;
          box-sizing: border-box;
          margin: 4px 0 4px 0;
          .th{
            width: 50%;
            height: 100%;
            line-height: 25px;
            display: inline-block;
            text-align: left;
            font-size: 12px;
            font-weight: normal;
            opacity: 0.5;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            vertical-align: middle; /*不设置会偏移*/
          }
          .td{
            width: 50%;
            height: 100%;
            line-height: 25px;
            display: inline-block;
            text-align: left;
            font-size: 12px;
            font-weight: normal;
          }
        }
        .select-time{
          width: 100%;
          .th{
            width: 70%;
          }
          .td{
            width: 30%;
          }
        }
      }
      .list{
        margin: 5px 0;
      }
    }
    .footer{
      width: 100%;
      height: 60px;
      .submit {
        float: right;
        width: 100px;
        height: 30px;
        display: block;
        border-radius: 2px;
        color: white;
        font-size: 13px;
        line-height: 30px;
        text-align: center;
        background-color: #06bf04;
        margin-top: 20px;
      }
    }
  }
</style>
<template>
  <view class="main-box">
    <zan-toast id="zan-toast-test"></zan-toast>
    <UserInfo :userData="userData"></UserInfo>
    <view class="list block">
      <view class="title detail">
        订单列表详情
      </view>
      <view class="content list">
        <repeat for="{{carListAll}}" key="index" index="index" item="item">
          <view id={{item.id}} class="tr">
            <view class="th">{{item.goodsName}}</view>
            <view class="td">数量：{{item.count}}</view>
          </view>
        </repeat>
      </view>
    </view>
    <view class="footer">
      <view @tap="addOrder" class="submit">提交订单</view>
    </view>
    <Loading :show.sync="showLoading" message="正在加载"></Loading>
  </view>
</template>

<script>
  import wepy from 'wepy'
  import { connect } from 'wepy-redux'
  import api from '../../service/api'
  import router from '../../mixins/router'
  import Loading from '../../components/loading'
  import UserInfo from '../../components/userInfo'
  import { UPDATE_LIST, DELL_ALL_LIST } from '../../store/types'
  import { OriginCode } from '../../utils/utils'
  const Toast = require('../../zanui/toast/toast')

  @connect({
    getSelectList(state) { // 类似getState
      return state.selectList
    }
  }, {
    update_list: UPDATE_LIST,
    dell_all_list: DELL_ALL_LIST
  })

  export default class Confirm extends wepy.page {
    config = {
      navigationBarTitleText: '检查订单',
      usingComponents: {
        'zan-date-picker': '/zanui/datetime-picker/index',
        'zan-toast': '/zanui/toast/index'
      }
    }

    components = {
      Loading: Loading,
      UserInfo: UserInfo
    }

    data = {
      totalNum: '',
      carListAll: [],
      currentPage: 1,
      pageSize: 15,
      zanToast: {
        title: '请选择时间',
        show: false
      },
      userData: {
        contactName: '邢必成',
        contactTel: '1234567890',
        customerName: '西南科技大学',
        pinyinfieldname: '请尽快送达'
      },
      is_empty: false,
      showLoading: false,
      pages: '',
      show: true,
      showModalStatus: false,
      goodsCount: 0,
      internalTradeId: 0,
      index: -1,
      timer: '',
      showWarning: false,
      waringMsg: ''
    }

    methods = {
      async addOrder() {
        if (!this.timer) {
          Toast({
            message: '请选择时间',
            type: 'fail',
            selector: '#zan-toast-test'
          })
        } else {
          Toast({
            message: '正字提交',
            type: 'loading',
            selector: '#zan-toast-test'
          })
          this.setData({ showLoading: true })
          const { customerName } = this.data.userData
          const { data: orderNo } = await this.getOrderNo()
          const orderDetailList = await this.getOrderDetailList()
          const lumpSum = this.getLumpSum(orderDetailList)
          const newOrderData = {
            verified: 0,
            origin: OriginCode.WECHAT_ORDER.value,
            customerName,
            customerType: '涪城区',
            orderNo: orderNo,
            customerTypeId: '2',
            distributionDate: this.timer,
            realDistributionDate: this.timer,
            lumpSum,
            orderDetailList
          }
          const json = await api.WXOrderAdd(
            newOrderData
          )
          this.showLoading = false
          if (json.code === 0) {
            Toast({
              message: '提交成功',
              type: 'success',
              selector: '#zan-toast-test'
            })
            this.handleDeleteAll() // 提交之后删除所有购物车中商品
          }
          this.$apply()
        }
      }
    }

    events = {
      'change': (e) => {
        const dateArr = e.detail.value
        this.timer = `${dateArr[0]}-${dateArr[1]}-${dateArr[2]}`
      }
    };

    mixins = [router]

    onShow() {
      this.carListAll = []
      this.getCar()
    }

    onLoad() {

    }

    filter(arr) {
      for (let i = 0; i < arr.length; i++) {
        let item = arr[i]
        if (item.count === 0) {
          arr.splice(i, 1)
          i--
        }
      }
      return arr
    }

    // 获取购物车中的所有商品
    async getCar(currentPage) {
      const json = await api.CarList({
        currentPage: currentPage || 1,
        pageSize: 20
      })
      if (json.code === 0) {
        json.data.list = this.filter(json.data.list) // 滤掉数量为0的商品
        this.carListAll = [...this.carListAll, ...json.data.list]
        this.pageNum = json.data.pageNum
        this.pages = json.data.pages
        if (json.data.total === 0) { // 总共选了多少货品
          // 暂无数据
          this.is_empty = true
        }
      }
      this.$apply()
    }

    async getOrderNo() {
      return await api.OrderGetnum()
    }

    async getOrderDetailList() {
      return await Promise.all(this.carListAll.map(async item => {
        const orderDetail = await api.GoodsGet({id: item.goodsId})
        const { data: { code, goodsName, goodsType, unitName, price } } = orderDetail
        const { count } = item
        return {
          isNewRecord: false,
          inGoodsId: code,
          inGoodsName: goodsName,
          inGoodsType: goodsType,
          unitName: unitName,
          unitPrice: price,
          amount: count,
          selfAmount: count,
          sum: count * price,
          selfSum: count * price
        }
      }))
    }

    getLumpSum(orderDetailList) {
      let lumpSum = 0
      orderDetailList.forEach(function(item) {
        lumpSum += item.sum
      })
      return lumpSum
    }

    async handleDeleteAll() {
      const json = await api.CarDelAll({})
      if (json.code === 0) {
        wepy.$store.dispatch({
          type: DELL_ALL_LIST
        })
      }
    }

    onReachBottom() {
      // 判断总页数是否大于翻页数
      if ((this.pages) > this.pageNum) {
        // 防止重复加载
        this.pageNum++
        this.getCar(this.pageNum)
      } else {
        this.isNomore = true
      }
    }
  }
</script>
